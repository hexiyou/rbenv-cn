Gem::Specification.new do |s|
  s.name = "rbenv-cn"
  s.version = "1.1.0"
  s.date = "2022-04-11"
  
  s.summary = "rbenv-cn: install `rbenv` and `rbenv-cn` plugin quikly for Chinese users. "

  s.description = <<DESC
This command line tool `rbenv-cn` helps to install `rbenv` and `rbenv-cn` plugin quikly for Chinese users.
DESC

  s.license = "MIT"

  s.authors = "ccmywish"
  s.email = "ccmywish@qq.com"
  s.homepage = "https://gitee.com/RubyKids/rbenv-cn"

  s.files = [
  ]

  s.bindir = "exe"
  s.executables = ["rbenv-cn"]

  s.metadata = {
    "bug_tracker_uri"   => "https://github.com/RubyKids/rbenv-cn/issues",
    "source_code_uri"   => "https://github.com/RubyKids/rbenv-cn"
  }

end