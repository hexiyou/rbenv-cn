<div align="center">

# rbenv-cn

[![Say Thanks!](https://img.shields.io/badge/Say%20Thanks-!-1EAEDB.svg)](https://saythanks.io/to/ccmywish)

</div>

## 概述

`rbenv-cn` 使用
1. Gitee官方提供的ruby-build镜像
2. Ruby China官方镜像 [https://cache.ruby-china.com/](https://cache.ruby-china.com/)

帮助您在国内网络环境下的类Unix系统上编译，安装多版本Ruby，保证不需要使用任何外网(目前只能保证CRuby, 而 mruby, JRuby 等不支持)。

本项目起了两个作用

1. 作为`rbenv`(包含`ruby-build`)的国内一键安装/卸载脚本，安装仅需4秒
2. `rbenv-cn`作为`rbenv`的一个标准插件(包含4个命令)来工作，切换使用国内镜像来下载源代码

**注意! Ruby用户可以直接通过RubyGems来安装，因此您只需要记住rbenv-cn这个名字即可**，安装方式请阅读下述内容。

<br>

> **如果您长时间使用本项目，请考虑捐赠作者(本页最下方)，谢谢**

<br>

## 功能

支持Bash, Zsh暂不支持Fish, PowerShell. 若您有新的想法，或发现了存在的问题，或可改善的地方(比如对新Shell的支持，其他插件的补充)，或者您想直接参与开发与日常维护，请您在[issues](https://gitee.com/RubyKids/rbenv-cn/issues) 处反馈，并请阅读[CONTRIBUTING.md](./CONTRIBUTING.md)

**镜像说明:**

1. `rbenv`最低两周一次手动更新，此是否更新几乎不影响使用
2. `ruby-build`已由Gitee官方管理(查看[最新状态](https://gitee.com/mirrors/ruby-build)),每天更新,这是管理Ruby版本的真正仓库,所以不用担心更新不及时
3. `ruby-build`的过程使用的是 [https://cache.ruby-china.com/](https://cache.ruby-china.com/) 即Ruby China官方的镜像

<br>

## 截图

- `rbenv-cn` 安装过程,如图所示仅需4秒。

![`rbenv-cn` 安装过程](./images/install.png)

<br>

- `rbenv cninstall <version>` 安装Ruby过程

![`rbenv cninstall` 安装Ruby过程](./images/cninstall.png)

<br>

## 测试状态

- [Linux各发行版测试](https://gitee.com/RubyKids/rbenv-cn/issues/I4YNS9)
- [macOS测试](https://gitee.com/RubyKids/rbenv-cn/issues/I4YNSI)
- [WSL测试](https://gitee.com/RubyKids/rbenv-cn/issues/I4YNS1)
- [国内服务器测试](https://gitee.com/RubyKids/rbenv-cn/issues/I4YNSO)

需要您帮助测试反馈情况，若您长时间使用`rbenv-cn`，请考虑成为维护者，谢谢。

<br>

## 使用

`ruby-build` 并不检查编译环境，需要手动先安装好。请先查看下方的编译依赖。
[编译依赖](https://github.com/rbenv/ruby-build/wiki#suggested-build-environment)

```bash
# 在rbenv的基础上，可以调用三个子命令 
# 1. `rbenv cninstall` 
# 2. `rbenv update`
# 3. `rbenv sudo`

# 当有新Ruby版本出现时, 使用rbenv update来同时更新 
# rbenv 以及 ruby-build, rbenv-cn 在内的所有插件
rbenv update

# 查看支持的Ruby版本
rbenv cninstall -l 或 -L 

# 此命令自动从Ruby China提供的镜像上下载某指定版本的Ruby并接着运行编译等过程
# 该命令用来替换常见的 rbenv install 3.2.0-preview1
rbenv cninstall 3.2.0-preview1

# 设置全局使用 3.1.0 版本
rbenv global 3.1.0

# 生成 .ruby-version 文件，设置在本目录下使用 3.1.0 版本(会覆盖全局设置)
rbenv local 3.1.0

# sudo rails s -p 81 等需要更高权限的操作无法执行, 这是RVM和rbenv共同的问题
# 因此需要使用以下命令来替换 sudo 即:
rbenv sudo rails s -p 81

```

<br>

## 安装

如果您的系统已经安装Ruby，您可以更简单
```bash
# 安装此Gem
gem install rbenv-cn

# 安装
rbenv-cn install

# 卸载
rbenv-cn uninstall
```

或通过下面的安装脚本一键安装/卸载
```shell
# 安装
bash -c "$(curl -fsSL https://gitee.com/RubyKids/rbenv-cn/raw/main/tools/install.sh)"

# 卸载
bash -c "$(curl -fsSL https://gitee.com/RubyKids/rbenv-cn/raw/main/tools/uninstall.sh)"
```

<br>

上述安装过程一般不会有问题，如果您发现异常，可以使用以下两种方式检查:
```bash
rbenv doctor 
# 或 
curl -fsSL https://gitee.com/RubyKids/rbenv-cn/raw/main/bin/rbenv-doctor | bash
```

<br>

## Gem换源
```bash
gem source -r https://rubygems.org/ 
gem source -a https://gems.ruby-china.com 

# Bundler change source
bundle config 'mirror.https://rubygems.org' 'https://gems.ruby-china.com' 
```

<br>
